# Access Control
One of the goals of computer security is to achieve:
- enabling legitimate users to access resources in an authorized manner
- preventing legitimate users from accessing resources in an unauthorized manner
- preventing unathorized users from gaining access to resources

Key concepts:
- security policy: a set of high-level laws definig what is allowed
- security model: formal representation of a securoty policy
- security mechanism: low-lwvwl SW/HW functions that implements the policy and the model

## Principles

### Definition
> The process of granting or denying specific requests to:
> - obtain and use informations and related information processing services
> - enter specific physical facilites
>
> or
>
> A process which use of sstem resources is regulated by a security policy and its permitted only by authorized entities

### Context
- Authentication: verify the validity of the credentials of the a user or of another system
- Access control (Authorization): determines if the specificrequested access is permitted
- Audit: monitors and keep track of users and system activities

![Access Control Model](pictures/access_control_model.png)

### Function
The access control module must have the following properties:
- tamper-proof: inalterable
- non-bypassable
- security kernel: confined in a well defined part of the system
- small: the smaller, the easier to verificate

The access control module is usually divided in:
- Policy Enforcement Point (PEP): enforce the security policy
- Policy Decision Point (PDP): make decisions being aware of the security policy

### Components
- Subject:
  - entity capable to access an object
  - accountable for its actions
  - 3 classes: Owner, Group and World
- Object:
  - a resource which access is controlled
- Access Right:
  - description on how a subject can access an object
  - include: Read, Write, Execute, Delete, Search

## Discretionary Access Control (DAC)
Usually used by OS and DB using an access matrix:
- one dimension consist of the authenticated subjects
- the other lists the objects
- each entry indicates the access rights (or privileges) of a specific subject for a specific object

![Access Matrix](pictures/access_matrix.png)

Because the access matrix is sparse, it is implemented by decomposition in two ways.

### Access Control List
![Access Control List](pictures/access_control_list.png)

Convenient when it is needed to determine which subjects have access to specific file.

### Capability List
![Capability List](pictures/capability_list.png)

Convenient to determine access rights available to a specific subject.

### Authorization Table
![Authorization Table](pictures/authorization_table.png)

Not sparse and more convenient that Access Control List and Capability Ticket, contains one row for each access right of one subject to one file.
Sorting by object = Access Control List.
Sorting by subject = Capability Ticket.
Can be easily implemented with a relational DB.

## Mandatory Access Control
![Mandatory Access Control](pictures/mandatory.png)

## Role-Based Access Control
Allows to specify enterprise-specific security policies in a way that maps naturally to an organization's structure. So it give more importance to the user responabilities rather than user's identity (that become relevant only for accountability). Usually roles corresponds to job functions.

RBAC implements the least privilege principle:
- each role should contain only the minimum set of access right needed
- a user is assigned to a role that enables him or her to perform only what is required
- users with the same role share the same access rights

![Users Roles and Resources](pictures/users_roles.png)

- users-roles relation is many to many
- users are dynamically
- roles are relatively static (only occasional changes)
- each role have specific access rights
- resources and relative access rights change infrequnetly

Also the RBAC two matrixes: one to relate users and roles, and one to relate roles and objects.

![Users Roles Table](pictures/users_roles_table.png)
![Roles Objects Table](pictures/roles_objectives_table.png)

### RBAC Reference Model
![RBAC Reference Model](pictures/rbac_reference.png)

#### RBAC 0: Base Model
Define:
- User: individual accessing the computer system
- Role: named job function 
- Permission: approval of a particular mode of access to some objects
- Session: mapping between a user and a subset of the set of roles to which the user is assigned

![RBAC0: Base Model](pictures/RBAC0.png)

#### RBAC 1: Role Hierarchies
Provide a mean to reflect the structure of roles in an organization.
- the higher the level, the greater the responsability
- subordinates have a subset of the access rights of the superior
- Inheritance: one role include access rights of the subordinate role

#### RBAC 2: Constraints
Adapts RBAC to the specifics of administrative and security policies.
A constraint is a relationship among roles or a condition related to roles.
There are three type of constraints: 
- Mutually exclusive roles: separation of duties and capabilities
- Cardinality: setting a maximum number with respect to roles
- Prerequisite roles: a user can be assigned to a particular role only if it is alredy assigned to some other specified role

### Advantages and Disadvantages
| Advantages                                                                | Disadvantages                                                 |
| ------------------------------------------------------------------------- | ------------------------------------------------------------- |
| to new users are assigned the needed roles                                | hierarchical relations among roles are not enought expressive |
| a change of work need only a change of role                               | propagation of privileges should be supported                 |
| new task of the system require only the decision of the role to manage it | identity of role's requestor should be verifies               |

## Attribute-Based Access Control
Define authorization that express conditions on attributes of both resource, subject and enviroment. This led to high flexibility and expressive power.

### Key Elements

#### Attributes
Are characteristic that define specific aspects of subjects, objects, enviroment conditions and operations.
An attribute contains informations about:
- class of informations (given by the attribute)
- name
- value

e.g.
```
Class = HospitalRecordsAccess
Name = PatientInformationAccess
Value = BusinessHoursOnly
```
##### Type of attributes
- Subject attributes
  - active entity that cause an information flow among objects or changes
- Object attributes
  - passive entity containing or receiving informations
- Enviroment attributes
  - describe the operational, technical and even situational enviroment or context in which the access occurs

#### Logical Architecture
![Logical Architecture](pictures/abac_arch.png)

#### Policy Model
Set of rules and relationships regulating the allowable behaviour based on:
- privileges of the subject
- how resources should be protected
- under which enviromental conditions

Policies are usually written from the point of view of the object to be protected.

A policy model is composed by:
- Subjects, Objects and Enviroment
- predefined attributes for S, O and E
- attribute assignement relation 
- a Policy Rule, deciding whenever an S can access an O in a particular E, that is a boolean function
- a Policy Store, made of policy rules

The access control decision process is an evaluation of the applicable policy rules in the policy store.

### Advantages
- fine-grained and versatile access control
- can enforce DAC, MAC and RBAC