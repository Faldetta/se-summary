# Database Security

## DBMS Architecture
![DBMS Architecture](pictures/dbms_arch.png)

## Need of DB Security
- contains snsitive data
- should be available to userbase
- DB has security requirements that are beyond the capability of a typical OS-based security mechanism
- DB allows a more detailed access control to be specifies
  
$`\implies`$ security services and mechanisms should be designed specifically.

## SQL Injection Attacks (SQLi)
Sends malicious SQL commands to the DB server, usally used for data extraction.

![SQLi](pictures/sqli.png)

SQLi works by:
- prematurely terminating a string and apending a (injected) SQL command
- terminating the injected string with comment mark `--` or `#`

### Example
Consider a query builder script working by string composition with user defined input:
```SQL
var ShipCity;
ShipCity = Request.form (“ShipCity”);
var sql = “SELECT * FROM OrdersTable WHERE ShipCity = ‘” + ShipCity + “’”;
```

The intention of the script’s designer is that when the script is executed, the user is prompted to enter the name of a city which is then assigned to `ShipCity`.


If the user enters Redmond, then the following SQL query is generated:
```SQL
SELECT * FROM OrdersTable WHERE ShipCity = ‘Redmond’
```
that selects all records in OrdersTable where attribute `ShipCity` is `Redmond`.

But suppose that the user enters:
```SQL
Redmond’; DROP table OrdersTable--
```
The result is the following query:
```SQL
SELECT * FROM OrdersTable WHERE ShipCity = ‘Redmond’;
DROP table OrdersTable--
```

`;` separates two inputs, `--` is an indicator that the remaining text of the current line is a comment.

So by tiping some special characters such as `'` and `--`, the user can change the semantics of the SQL statement.
This imply that the channel between the user and the database must be properly protected.

### Attack Approaches
- user input: injection by user input.
- server variables: forge of HTTP and network header's values.
- second order injection: injection of data from the database to attack, so no user is the cause.
- cookies: injection by parsing an altered cookie.
- physical user input: injection by physical object such as RFID.

### Attack Types

#### Inband
Use the same channel to inject SQL code and retrieving results. It may include the following aspects:
- Tautology: injects code in one or more conditional statements so that the result is true
- End-of-Line comment: inject code and nullify the following code through the use of line comments
- Piggybacked queries: the attacker injects additional queries beyond the original one
e.g.
```SQL
-- original scripts
query = “SELECT info FROM user WHERE name =’$_GET[“name”]’ AND pwd = ’$_GET[“pwd”]’”;
-- script with injection of "’ OR 1=1 --"
query = “SELECT info FROM user WHERE name =’’ OR 1=1 -- ’ AND pwd = ’$_GET[“pwd”]’”;
```
The password check is disabled and the WHERE statement become a tautology, so the query returns all the elements of the table

#### Inferential
The attacker is able to reconstruct some informations by sending specific requests and observing the resulting behaviour of the server; there is no actual transfer change.
- illegal/logical incorrect queries: the error handling is often descriptive of possible vulnerabilities.
- blind SQL injection: knowledge is infered by the behaviour of the system in response of true/false queries
  - if the result is true: the system continue to work properly
  - if the result is false: the system differs from the normal behaviour
- so the attacker check data dependent conditions instead of asking for the data 

#### Out-of-Band
Data is retrieved by different channels(e.g. email with query results), this can be used when there are limitations on information retrieval, but outbound connectivity from the database server is lax.

### Cause of Vulnerability
Mixing data and code toghether is the cause of SLQi (and other attacks such as cross-site scripting (in HTML)).
The parser should separate data and code to not use the first as the second.

### Countermeasures

#### Defensive Coding
- input validation through type checking
- separation of data and code channels
- SQL DOM API to encapsulatequeries within type-checked API

#### Detection
- Signature based
- Anomaly based
- Code analysis through test suite for SQLi

#### Run-Time Prevention
Use automated tools to check queries at run-time.

## Database Access Control
Determine:
- portion of the database available to the users (from the entire DB to a row or a column)
- user's access rights (create, insert, ...)
  
Supports:
- centralized administration: users can grant and revoke some privileges
- ownership-based administration: owner can grant and revoke privileges to the table
- decentralized administration: as ownership-based + centarlized administration (for other privileged users)

### SQL Access Control
Commands to manage rights:
- `GRANT` (may enable a cascade access grant)
- `REVOKE` (may enable a cascade as well)

Access rights:
- `SELECT`
- `INSERT`
- `UPDATE`
- `DELETE`
- `REFERENCES`

e.g.
```SQL
GRANT SELECT ON ANY TABLE TO ricflair
REVOKE SELECT ON ANY TABLE FROM ricflair
```

#### Cascading Authorization

##### Granting
![Cascading Authorization Grant](pictures/SQL_cascade.png)

##### Revoking
![Cascading Authorization Revoke](pictures/SQL_cascade_revoke.png)

The revokation, like the grant, of privileges is made on cascade.

#### Role-Based Access Control
Categories of user: 
- application owner
- administrator
- end user

For each task, one or more role can be defined for the specific need.
The owner assigns roles to end users, admins are responsible for sensitive roles anc can assign administrative roles to end-users.

## Inference
Process of performing authorized queries to deduce unhauthorized information from the legitimate response received.
The attacker could use non-sensitive data or metadata to do this.

![Inference](pictures/db_inference.png)

Inference is done by:
- analyzing functional dependencies between attributes
- merging views with the same constraints

### Approaches to Inference
- Detection during design:
  - remove inference channels or changing access control policy
  - the stricter policy led to a reduction in availability
- Detection at query time:
  - if an inference  channel is detected, the query is denied or altered

### Inference Detection
- I security problem: can be detected with analysis of data structures and security constraints
- II security problem: cannot be detected using informations stored in the database

Inference detection algorithms are needed.

## Database Encryption
Disadvantages:
- Key management: authorized users must have access to decryption key for the datat for which they have access
  -  secure keys to selected parts of the database to authorized users $`\implies`$ complex task
- Inflexibilty: with partially encrypted datasets is difficult to perform searches

![Database Encryption Scheme](pictures/db_encr.png)

- items are encrypted separately with the same encryption key
- the encrypted database is on a server; the server doesn't have the key
- the client system has a copy of the key, so it can decrypt

### Flexible Encryption Scheme
- Each record (row) is encrypted as a block
  - $`B_i = (x_{i1}||\dots||x_{iM}) \implies E(k,B_i) = E(k,(x_{i1}||\dots||x_{iM}))`$
![Database Matrix](pictures/db_block.png)

- To assist data retrieval, attribute indexes are associated with each table
  - $`(x_{i1}||\dots||x_{iM}) \implies [ E(k,B_i), I_{i1},\dots, I_{iM} ]`$
![Database Encrypted with Indexes](pictures/db_block_enc.png)
  - this provide some informations to an attacker

## Data Center Security

![Data Center](pictures/datacenter.png)

Data centers stores a massive amount of data, so their security is a top priority.
The main threats are Dos, SQLi, Malware and Phisical threats.

Data centers security is represented with a four-layer model:
- Site security: physical security of the entire site (entry points, ...)
- Physical security: multi-factor authentication for physical access, mantrap (like bank's double door), ...
- Network security: inclusion of firewalls, anti-virus, intrusion prevention/detection, authentication, ...
- Data security: encryption, password policy, data protection, ...
  
TIA-942 standard enforce the security in these four areas.
It has four levels:
1. basic data center, no redundancy
2. redundant components: single power and cooling distribution path with redundant components
3. concurrently maintainable: multiple power and cooling distribution paths with only one active
4. fault tolerant: multiple active power and cooling distribution paths
