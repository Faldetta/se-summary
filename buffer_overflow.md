# Buffer Overflow

## Stack Overflow
> condition at an interface under which more input can be placed into a buffer or data holding area than the capacity allocated, overwriting other information. Attackers exploit such a condition to crash a system or to insert specially crafted code that allows them to gain control of the system.

### Example
```c
#include <string.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    const int FALSE = 0;
    const int TRUE = 1;
    int valid = FALSE;
    char str1[8];
    char str2[8];
    strcpy(str1,"START");
    gets(str2);
    if (strncmp(str1, str2, 8) == 0)
    valid = TRUE;
    printf("buffer1: str1(%s), str2(%s), valid(%d)\n", str1, str2, valid);
}
```
```sh
$ gcc -g -o buffer1 buffer1.c
$ ./buffer1
warning: this program uses gets(), which is unsafe.
START
buffer1: str1(START), str2(START), valid(1)
```
```sh
# ABC as input
– buffer1: str1(START), str2(ABC), valid(0)

# EVILINPUTVALUE as input
– buffer1: str1(TVALUE), str2(EVILINPUTVALUE), valid(0)
```

- gets() of the traditional C library does not include any check on the amount of data copied: reads the next line of text from the program’s standard input up until the first newline character occurs and copies it into the supplied buffer followed by the NULL terminator

- str2 is a string represented as an array of 8 characters (including NULL `\0`)
- if initialized using a function that does not control the input, like gets(), it could end up with more than 8 characters
- The first 8 characters will be stored in str2, the extra characters (including NULL) will be stored in the memory area adjacent to the one dedicated to str2; this causes overwriting the value of the adjacent variable, str1 in this case

```sh
# with BADINPUTBADINPUT as in put 
buffer1: str1(BADINPUT), str2(BADINPUTBADINPUT), valid(1)
```
- in this case the comparison result true: the C library function strncmp called in the program compares at most the first 8 characters of two NULL-terminated byte strings
-  dangerous possibility occurs if the values in these buffers were an expected password and a supplied password needed to access privileged features

### Carrying out Buffer Overflow Attacks
To exploit a buffer overflow it is needed: 
1. to identify a buffer overflow vulnerability in some program that can be triggered using externally sourced data under the attacker’s control
2. to understand how that buffer is stored in memory and determine potential for corrupting adjacent memory locations and altering the flow of execution of the program

The identification of vulnerable programs can be done by:
- inspecting program source
- tracing the  execution with oversized input
- using tools (like fuzzing) to automatically identify potentially vulnerable programs

### Machine & Assembly Languages
- manipulated data are stored in registers or in memory
- data are arrays of bytes
- programmers are responsible for the correct interpretation of data
- greatest access to the resources at the highest cost and responsibility in coding effort for the programmer

### High-Level Languages
- strong notion of type of variables and valid operations
- not vulnerable to buffer overflows as do not permit more data to be saved into a buffer
  
### C and Related Languages
- high-level control structures and data type abstractions, but allow direct access to memory data
- ability to access low-level machine resources means that the languages are susceptible to inappropriate use

### Stack Buffer Overflow
- also referred as stack smashing
- occurs when the buffer is located on the stack, usually as a local variable in the stack frame of a function

![Program Memory](pictures/program_memory.png)
![Stack Frame](pictures/stack_frame.png)

> Stack pointer: points to the first free location
> 
> Frame pointer: points to the beginning of the calling function’s stack frame, which is the location where the old frame pointer is saved

The core of a stack overflow attack is given by the possibility of overwriting the old frame pointer and return address.
Because local variables are placed below these key function linkage values, the possibility exists of exploiting a local variable buffer overflow vulnerability
to overwrite the values of one or both of them.

#### Example
```c
#include <stdio.h>

void hello(char *tag) {
    char inp[16];
    printf("Enter value for %s: ", tag);
    gets(inp);
    printf("Hello your %s is %s\n", tag, inp);
}

int main(int argc, char *argv[]) {
    hello("name");
}
```
```sh
$ ./buffer2
warning: this program uses gets(), which is unsafe.
Enter value for name: Bill and Lawrie
Hello your name is Bill and Lawrie

$ ./buffer2
warning: this program uses gets(), which is unsafe.
Enter value for name: ABCDEFGHQRSTUVWXabcdefgh
Hello your name is ABCDEFGHQRSTUVWXabcdefgh
Abort trap: 6
```
When the function attempts to transfer control to the return address, it typically jumps to an illegal memory location  `Abort trap: 6`

#### Observations
- scope of possible buffer overflow vulnerabilities is very large
- the potential for a buffer overflow exists anywhere externally sourced data are copied or merged into a buffer

- program must check to ensure that the buffer is large enough, or that the data copied are correctly terminated
- program can safely read and save input, pass it around the program, and then at some later time in another (library) function unsafely copy it, resulting in a buffer overflow

- consequence of a successful buffer overflow attack, significant changes are made to the memory of the attacked program, near the top of its stack frame
  - return address and pointer to the previous stack frame are usually destroyed
  - there is no easy way to restore the program state and continue execution, and the function or service provided by the attacked program is lost

- can be made more sophisticated
  - attacker could manage to have it transfer control to a location and code chosen by the attacker
  -  input causing the buffer overflow to contain the desired target address at the point where it will overwrite the saved return address in the stack frame

### Shellcode
Approach used in the original stack overflow was to include bunary values corresponding to the desired machine code in the buffer being overflowed.

The return address used in the attack is the starting adddress of this shellcode, which is a location in the middle of the targetted function's stackframe.

![Shellcode](pictures/shellcode.png)

Tipically transfers control to a command-line interpreter, which gives access to any program available on the system with the privileges of the attacked program.

#### Restrictions
- shellcode must be position independent in order to be able to run no matter where in memory it is located
- shellcode cannot contain any NULL values (otherwise, it would not be loaded entirely due to copy into a buffer by string manipulation routines)

#### Steps
1. Identify a suitable, vulnerable, trusted utility program
2.  Analyze the target program to determine the likely location of the targeted buffer on the stack and how much data are needed to reach up to and overflow the old frame pointer and return address in its stack frame (using a debugger)
3.  Define the attack string
4.  Specify the commands to be run

## Defending Against Buffer Overflow

### Compile-Time Defenses
Harden new programs to resist attacks

#### High-Level Programming Language
- Not vulnerable to buffer overflow attacks
- Compilers include additional code to enforce checks of value ranges and permissible operations on variables
- Disadvantages: 
  - Additional code must be executed at run time to impose checks (affect performances)
  - Loss of access to some instructions and hardware resources

#### Safe Coding Techniques
Programmers need to inspect the code and rewrite any unsafe coding (using standards like certC).

#### Language Extensions/Safe Libraries
Extend semantics to handle both statically allocated arrays (range check) and dynamically allocated memory; programs and libraries need to be recompiled with the new compiler

Replace standard Library with a safer version that cannot prevent corruptio of adjacent local variables, implemented with DLL to be loaded before the existing standard libraries.

- Advantages: 
  - performance overhead is negligible
  - does not require changes to the OS
  - does not need access to the source code of defective programs
  - works with existing binary programs without requiring their recompilation

#### Stack Protection
Relies on compiler extensions that insert additional code to functions entry and exit code to check the stack frame for any evidence of corruption (in that case abort).

StackGuard is a GCC extention (used to compile en entire Linux distro): 
- On function entry: the added code writes a random value below the old frame pointer address, before the allocation of space for local variables, and saves a copy of it (referred to as the canary) at a safer place that is off the stack
-  On function exit: the added code checks the canary against the value saved on the stack for modification before continuing with the usual function exit operations of restoring the old frame pointer and transferring control back to the return address

Stackshield & Return Address Defender are compiler extensions that do not alter the structure of the stack frame: 
- On function entry: the added code writes a copy of the return address to a safe region of memory that would be very difficult to corrupt
- On function exit: the added code checks the return address in the stack frame against the saved copy and, if any change is found, aborts the program

These startegies requires recompilation, but if the stack is not modified debuggers will work without no problem.

### Run-Time Defenses
Detect and block attacks in existing programs by changing memory management.

#### Executable Address Space Protection
Make some regions of memory non-executable, because executable code should only be found elsewhere in the processes address space.
It requires support from the HW.
It is one of the best method, but may be needed to support programs that need toplace executable code on the stack (JIT compiler).

#### Address Space Layout Randomization
Randomizes the layout of the memory of a program, making it difficult for attackers to guess the actual addresses.
Is a common countermeasure for buffer overflow vulnerability implemented at the OS loader program

#### Guard Pages
Are gaps placed between critical regions of memory (e.g. stack, heap, global data) in a process address space and are flagged as illegal addresses.

## Other Forms of Overflow Attacks
- variation on stack:
  - replacement stack frame: overwrite buffer and saved frame pointer address, but no return address, the current function returns to its calling function as normal, since its return address has not been changed. Saved frame pointer value is changed to refer to a dummy stack frame located near the top of the overwritten buffer
  - return-to-system call: return address is changed to jump to existing code on the system
- heap overflow: like the stack one, but targetting dynamic data structures
- global data overflow: attack buffers located in the program's global data (above program code)
- ...