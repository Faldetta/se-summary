# Operating System Security

System Abstract Layers:

![System Abstract Layers](pictures/abstract_layer.png)

Each of these layers of code needs adoption of appropriate hardening measures to provide appropriate security services.
Each layer is vulnerable to attacks from below, so the lower layers should be secured appropriately.

## Introduction to System Hardening
Because computer systems are central to the IT infrastructure is necessary to manage the installation and countinuing operations of these software systems.

Building and deploying a secure system should be a planned process designed to counter compromission during the installation process; the security should be mantained during its operational lifetime.

## System security planning
Because it is more difficult and expensive to reapair security at a later time, the inititial development phase need a good planning.

The planning:
- aims at maximizing security while minimizing costs
- should include a wide security assessment of the organization
- needs to determine security requirements for the system, applications, data, and users
- needs to identify appropriate personnel and training to install and manage the system

## Operating systems hardening

### Initial Setup and Patching
New systems should be constructed on a protected network, either isolated or restricted; the OS and the available patches should be transferred using trusted removable media.

Full installation and hardening process should occur before the system is deployed to its intended location.

The overall boot process must be secured (with adjusted options, BIOS password and a limited list of bootable media permitted).

Integrity and source of any additional device driver code must be carefully validated, since it executes with full kernel level privileges.

The system must be kept up to date, with all critical security related patches installed, maybe automating the process.

### Harden and Configure

#### Remove Unnecessary Services, Applications, Protocols
During the initial installation the supplied default configuration should not be used (because maxinixe usability and not security).

A system security planning process should identify what is actually required for a given system; only required packages should be installed, fewer SW packages are available to run, lesser is risk since lesser SW vulnerabilities can be contained in the system.

#### Configure Users, Groups, and Authentication
A system security planning process should identify:
- categories of users  
- needed privileges and types of data and resources the can access
- how and where they are defined

Elevated privileges should be restricted to only those users that need them and only when they are needed to perform a task.

The default accounts (part of the system installation) should be secured:
- removed if not required
- default passwords should be changed

Policies for authenticating credentials are configured with: 
- authentication methods
- required length and complexity (and allowed age for passwords)

A key decision is if the users, the groups they belong to and their authentication methods are specified locally on the system or on a centralized authentication server.

#### Configure Resource Controls
Once the users and groups are defined, appropriate permissions can be set on data and resources to match the specified policy thus limiting execution of programs and data read or write.

### Install Additional Security Controls
Further security improvement is possible by installing and configuring additional security tools (antivirus, firewall, ids, ips and whitelisted applications).

### Test the System Security
Ensure the previous security configuration steps are correctly implemented and identify any possible vulnerability.

- checklists are included in security hardening guides
- there are programs specifically designed to review and scan a system
- should be done periodically

## Application security
Required services and applications must be installed and configured, the steps are similar to those for the OS.

To reduce the possible vulnerabilities:
- only required SW should be installed
- carefully select, configure and update remote access or service

### Application Configuration
- may require specification of appropriate data storage areas for the application or service
- may require appropriate changes to the default configuration details
- access rights granted to the application should be carefully considered

### Encryption Technology
Used for securing data both in transit and when stored; if needed for the system, it must be configured and appropriate cryptographic keys created, signed, and secured.

Secure network service (both wit asymmetric key pairs):
- SSL/TLS or IPSec (with X.509 certificate)
- SSH (without X.509 certificate)

## Security Maintenance
Continuous process which takes place once the system is appropriately built, secured, and deployed; it is made by:
- Monitoring and analyzing logging information
  - Can only inform about bad things that have already happened
  - Key is to ensure to capture the correct data and then appropriately monitor and analyze this data
  - Automated analysis (as done by IDSs) is preferred
- Performing regular backups
  - Performing copies of data on a regular basis is critical to assist with maintaining the integrity of the system and user data
  - Backup: process of making copies of data at regular intervals, allowing the recovery of lost or corrupted data over relatively short time periods
  - Archive: process of retaining copies of data over extended time periods, in order to meet legal and operational requirements to access past data
- Recovering from security compromises
- Regularly testing system security
- Patch and update all critical software
- Monitor and revise configuration as needed

## Linux/Unix security

### Patch Management
Keeping security patches up to date (in a timely manner) is a widely recognized and critical control for maintaining security.

Unix and Linux distributions include tools for automatically downloading and installing software updates (aka the package manager: apt, yast, pacman, dnf).

Because update can led to instability, is better to run them on an identical test system.

### Application and Service Configuration 
Implemented using separate text files for each application and service, generally located in `/etc/`; individual user configurations that can override the system defaults are located in hidden “dot” files in each user’s home directory.

Organizations with larger numbers of systems may instead employ some form of centralized management (by means of a central repository of critical configuration files that can be automatically customized and distributed to the systems they manage).

### User, Group and Permissions
Any default or generic users supplied with the system should be checked, and removed if not required.

Accounts that are required, but are not associated with a user that needs to login, as the special system users created to run specific services (e.g. www for the Web server), should have login capability disabled.

Information on user accounts and group membership is usually stored in local files, like `/etc/passwd` and `/etc/group` (modern systems can also import these from external repositories).

Unix and Linux systems implement discretionary access control to all file system resources.

Other recommendations are: 
- limiting the access permissions to critical directories and files
- minimizing number and size of programs that set user (`setuid`) or group (`setgid`) to the program's owner or group resp., since they are key target for attackers

### Remote Access Control
Vulnerabilities in a network server could be triggered by a remote attacker.
To counter remote exploits, it is important to limit access to the system to only those strictly required services.
This can be done with one of the following methods

#### TCP Wrappers library and tcpd daemon
Lightly loaded services may be “wrapped” using the tcpd daemon, which:
- listens for connection requests on their behalf
- checks that any request is permitted by the policy configuration files (`/etc/hosts.allow` and `/etc/hosts.deny`)
- if the request is accepted, then invokes the server program to handle it
- otherwise, logs the (rejected) request

More complex and heavily loaded services incorporate this functionality into their own connection management code (using the TCP Wrappers library) and the same configuration files

#### Host-based firewall SW
Linux systems mainly use the `iptables` SW to configure the `netfilter` kernel module.
BSD-based systems (including MacOS) use the `pf` SW with similar capabilities.

Most systems provide administrative utilities to generate common configurations and to select which services will be permitted to access the system; these should be used unless non-standard requirements needs to be addressed

### Logging and Log Rotation
Most applications can be configured to log information with levels of detail ranging from “debugging” (maximum detail) to “none” (while a middle setting is usually the best choice).

Usually it is permitted to specify dedicated file to write data of relevant events for the application or a facility (category) code to label log data written in a central repository (e.g. `/dev/log`) using the syslog protocol.

The `logrotate` utility  eases administration of systems that generate large numbers of log files. “Log rotation” refers to the practice of archiving an application’s current log, starting a fresh log, and deleting older logs.

### Application Security Using a Chroot Jail 
Some network accessible services do not require access to the full file-system, but rather only need a limited set of data files and directories.

A chroot jail is a mechanism that restricts the service’s view of the file system to just a specified portion.

Advantages: limit the consequences of a compromise, but it is not suitable for all circumstances, and it is not a complete security solution.

Disadvantages: 
- Needed files and directories must be copied into the chroot jail
- Determining what needs to go into the jail for the service to work properly can be tricky
- Correct configuration of a chroot jail is difficult; if created incorrectly, the program may either fail to run correctly or may be able to interact with files outside the jail

### Security Testing
A security checklists for several Unix and Linux distributions should be followed.
There are also many commercial and open-source tools available to perform system security scanning and vulnerability assessment (like `nmap`).

## Windows security
The “Windows Update” service and the “Windows Server Update Services” assist with the regular maintenance of Microsoft software, and should be configured and used.

Third-party applications also provide automatic update support, and these should be enabled for selected applications.

### Users Administration and Access Controls
Users and groups in Windows systems are defined with a Security ID, stored locally in the Security Account Manager; it may also be centrally managed for a group of systems belonging to a domain, with the information supplied by a central Active Directory.

Windows systems implement the discretionary access control model to access system resources.

The major differences between Linux and Windows DAC are:
- Linux: use a small number of access rights for subjects being owner/group/other across nearly all resources/objects
  - simpler model
  - the meaning of rigth differ for different objects
  - hard to specify complex requirements
- Windows: use a much larger set of access rights, which differ for different types of objects
  - more complex model
  - allow better specification for complex requirements
  
Windows privileges are defined system wide; hardening the system can include limiting the rights and the privileges granted to users and groups on the system.

To provide additional security and access policy granularity on shared resources one can use 
- share permissions: permissions (full, change or read) one sets for a folder hen she shares that folder
- NTFS permissions: determine the action users can take for a folder or file both locally and across the network
The most restrictive permission applies when share and NTFS permissions conflict.
 
Newer Windows systems provide:
- Mandatory integrity controls: whenever data is written to an object, the system first ensures that the subject’s level is equal or higher than the
object’s level (after all users and objets are being labelled)
- User Account Control: ensure users with administrative rights only use them when required
- Low Privilege Service Accounts: used for long-lived service processes, such as file print and DNS services, that do not require elevated privileges

### Application and Service Configuration
Configuration information is centralized in the Registry, a database of keys and values that may be queried and interpreted by applications.

Changes to the Registry can be made within specific applications or using the “Registry Editor”; these changes may also be recorded in a central repository and pushed out whenever a user logs in to a system within a network domain.

### Other security Controls
Given the predominance of malware that targets Windows systems, it is essential that suitable anti-virus, anti-spyware, personal firewall, and other malware and attack detection and handling software packages are installed and configured on such systems.

Newer Windows systems include basic firewall and malware countermeasure capabilities.

Business Windows versions support also a range of cryptographic functions.

### Security Testing
Security checklists for various versions of Windows should be followed.

There are also a number of commercial and open-source tools available to perform system security scanning and vulnerability assessment of Windows systems.

## Virtualization security
Virtualization is a technology that provides an abstraction of the resources
used by some SW, we are interested in full virtualization which allows multiple full OS instances to execute on virtual HW, supported by a SW hypervisor that manages access to the actual physical HW resources.

Benefits include:
- better efficiency in the use of the physical system resources
- support for multiple distinct OSs and associated applications

However additional security concerns raises because of multiple OSs executing side by side and the presence of the virtual machines and hypervisor and the security services they provide.

### Virtualization Basics
The hypervisor is SW that sits between the HW and the Virtual Machines (VMs) and acts as a resource broker; it allows multiple VMs to safely coexist on a single physical host and provides an abstraction of all physical resources.
 
Each VM includes an OS, called the guest OS that supports a set of standard library functions and other binary files and applications

From the point of view of applications and users, a computing stack appears as an actual machine, with HW and an OS

![Virtualization](pictures/virtualization.png)

The principal functions of an hypervisor are:
- Management of VMs execution, includes
    - scheduling 
    - virtual memory management 
    - emulation of timer and interrupt mechanisms
- Devices emulation and access control, includes
    - emulating all network and storage devices 
    - mediating access to physical devices
- Execution of privileged operations for guest VMs on behalf of the host HW
- VM lifecycle management, includes
    - configuring VMs
    - controlling VM states 
- Administration of hypervisor platform and hypervisor SW
    - Setting of parameters for user interactions with the hypervisor host as well as
    hypervisor SW

#### Hypervisor (Virtualization) Types

###### Type 1 Hypervisor (native virtualization systems)
The hypervisor executes directly on the underlying HW.

![Type 1 Hypervisor](pictures/type_1_hyper.png)

The hypervisor is loaded as a SW layer directly onto a physical server, like an OS is loaded; this imply that can directly control the physical resources of the host.

Once it is installed and configured, the hypervisor is capable of supporting VMs as guests.

In environments where virtualization hosts are clustered together for increased service availability and load balancing a hypervisor is staged on a new host, than  that new host is joined to an existing cluster, and VMs can be moved to the new host without any interruption of service.

##### Type 2 Hypervisor (or hosted virtualization systems)
The hypervisor executes as just another application on a host OS that is running on the underlying HW.

![Type 2 Hypervisor](pictures/type_2_hyper.png)

The hypervisor runs as a SW module on top of an OS, named host OS, exploiting its resources and functions.

Relies on the host OS to handle all the HW interactions on the hypervisor’s behalf.

##### Differences
Type 1 hypervisors typically perform better and is more secure than type 2, however  Type 2 hypervisors allow a user to exploit virtualization without needing to dedicate a server to only that function.
Moreover native virtualization systems are typically seen in servers, and are used for improving HW efficiency, while hosted virtualization systems are more common in
clients, where they run alongside other applications on the host OS and are used for supporting applications for alternate OS versions or types.

#### HW Resources in Virtualized Systems
Available HW resources must be appropriately shared among the various guest OSs.
- CPU and memory are generally partitioned between the guest OSs and scheduled as required
- Disk storage may be partitioned, with each guest OS having exclusive use of some disk resources or virtual disks may be created (seem like a real disk to the guest OS and like a disk image to the host one)
- Attached devices are generally allocated to a single guest OS at a time
 
#### Network Access in Virtualized Systems
These are the possible alternatives: 
- guest OS may have direct access to distinct network interface cards
- hypervisor may mediate access to shared NICs
- hypervisor may implement virtual NICs for each guest OS, bridging or routing traffic between guest OSs (most efficient, but with security consequences since this traffic is not subject to monitoring by probes in the phisical network)

### Containers (or Application) Virtualization
Recent approach to virtualization where a SW known as container runs on top of the host OS kernel and provides an isolated execution environment for applications.

![Container](pictures/container.png)

Support for the containers is given by a container engine:
- it sets up each container as an isolated instance by requesting dedicated resources from the host OS for each container
- each containerized application then directly uses the resources of the host OS
- All containerized applications on a host share a common OS kernel, which greatly reduces overhead

### Hypervisor vs Container Virtualization
- Hypervisor-based virtualization functions at the border of HW and OS
  - provide strong isolation and security guarantees with the narrowed interface between VMs and hypervisors
  - guest OS runs on top of the virtualization SW
- Container-based virtualization sits in between the host OS and applications
  - incurs lower overhead since all containerized applications on a host share a common OS kernel, thus avoiding to dedicate resources to run a separate OS for each application
  - potentially introduces greater security vulnerabilities
  - no guest OS runs on top of the virtualization SW

### Securing Virtualization Systems
Pplan the security of the virtualized system:
- Ensure that the virtualization SW is properly secured
- Secure all elements of a full virtualization solution and maintain their security
- Restrict and protect administrator access to the virtualization solution

#### Virtualization Security Issues
Securing virtualized systems means extending the security process to the virtualized environment and the hypervisor.
The presence of the virtualized environment and the hypervisor may reduce security if vulnerabilities exist inside them, which attackers may exploit (Such vulnerabilities could allow applications executing in a guest OS to covertly access the hypervisor).

#### Hypervisor Security
When using virtualized systems, we must also deal with the following security concerns in addition to previously seen:
- Guest OS isolation: ensuring that programs executing within a guest OS may only access and use the resources allocated to it
- Guest OS monitoring (by the hypervisor): The hypervisor has privileged access to the programs and data in each guest OS
- Virtualized environment security: (aka snapshot and image management) virtualized systems usually provide support for suspending an executing guest OS in a snapshot, saving that image, and then restarting execution at a later time (if attackers can view or modify this image, they can compromise the security)

The hypervisor should be secured using a process similar to securing an OS.

Because an hypervisor may support both local and remote administration, suitable authentication and encryption mechanisms dhould be used.

#### Virtualized Infrastructure Security
- Access to HW resources, managed by a hypervisor, must be limited to just the appropriate guest OSs that use any resource
- Access to VM images and snapshots must be carefully controlled
- Network connections should be suitably arranged to isolate and protect network traffic by using several different network segments

NIST distinguish three distinct categories of network traffic:
- Management traffic: used for hypervisor administration and configuration of the virtualized infrastructure
- Infrastructure traffic: such as migration of VM images, or connections to network storage technologies
- Application traffic: between applications running on VMs and external networks

#### Virtual Firewall
Provides firewall capabilities for the network traffic flowing between systems hosted in a virtualized environment that does not require this traffic to be routed out to a physically separate network supporting traditional firewall services.

These capabilities may be provided by a combination of
- VM Bastion Host: A separate VM is used supporting the same firewall systems and services that could be configured to run on a physically separate bastion host, including possibly IDS and IPS services
- VM Host-Based Firewall: Firewall capabilities provided by the guest OS running on the VM and configured to secure that host, in the same manner as used in physically separate systems
- Hypervisor Firewall: Firewall capabilities are provided directly by the hypervisor (more secure and efficient)

#### Hosted Virtualization Security
Hosted virtualization systems pose some additional security concerns due to:
- the presence of more layers to secure 
- the fact that the users of such systems often have full access to configure the hypervisor, and to any VM images and snapshots

In fact, virtualization is typically used more to provide additional features and to support multiple OSs and applications, than to isolate these systems and data from each other.
