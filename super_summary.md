# Overview
## Key Concepts
- Confidentiality
- Integrity
- Availability
- Authentication
- Accountability

# Cryptographic Tools
## Symmetric Encription
### Block
#### Electronic Code Book 
#### Cipher Block Chaining
### Stream
## Message Authentication
### Message Authentication Code
### Message Digest
#### Message Digest Authentication
#### Hash Functions Properties
- Applied to block of any size
- Produce output of fixed length
- Hash is easy to compute
- One Way: infeasible to finding a message with a given digest
- Weak collision resistant: infeasible to finding a message with the hash of another message
- Strong Collision resistant: infeasible to finding a pair of messages with the same hash
## Public-Key Encryption
- Public key to cipher: confidentiality
- Private key to cipher: message authentication and integrity
- one-way
- trap-door one way
### Digital Signature
### Public Key Certificate
- Public Key Infrastructure
- Trust problem
### Digital Envelopes

# User Authentication
## Password (something known)
### Salted Hash
### Dictionary Attack
### Rainbow Table
### Password Selection
- Rules
- Computer Generated
- Reactive
- Proactive
#### Bloom Filter
## Token (Something owned)
### Smart Tokens
- Static
- Dynamic
- Challenge-Response
### Smart Cards
## Biometric, Static and Dynamic (something that is or is done)
### Enrollment and Verification/Identification
### Testing (user and imposter profile)

# Access Control
## Discretionary Access Control
### Access Control List
### Capability List
### Authorization Table
## Mandatory Access Control
## Role-Based Access Control
### RBAC Reference Model
## Attribute-Based Access Control

# Database Security
## SQL Injection 
### Types
- Inband: same channel for input and output
- Out-of-band: data retrieved by a different channel
- Inferential: information reconstructed by db behaviour or view creation
### Countermeasures
- Defensive Coding: input validation
- Detection
- Run Time: query check
## Database Access Control
## Database Encryption

# Malicious Software
## APT
## Propagation
### Virus
### Worm
### Spam
### Trojan
## Payload
### System corruption
### Bot or zombie
### Spyware, Keylogger, Phishing  and reconnaisance
### Backdoor and Rootkits
## Countermeasures
### Prevention
### Mitigation
### Detection
#### Host-based
- I simple scanner: based on malware signature (fingerprint)
- II heuristic scanner: based on heuristic rules to find probable malware instances
- III activity traps: memory-resident programs identifying malware by actions
- IIII full-featured protection: variety of techniques used in conjuction; this include sophisticated approaches like sandbox analysis and Host-Based Dynamic Malware Analysis
#### Perimeter
- IDS and firewalls, scan content but not behaviour
#### Distributed Intelligence Gathering
- Host+Perimeter

# Denial of Service
## Flooding
## SYN spoofing
## DDoS
## Reflecion and Amplification
### DNS amplification
## Application-based Bandwith attack
- resouce consumption at a server with regard to the attack effort
### Session Initiation Protocol
### HTTP flooding
## Defences
- Prevention
- Detection and filtering
- Traceback and identification
- Reaction

# Intrusion Detection
## Approaches
### Anomaly Detection
### Signature Based
## Topology
### Host-Based Intrusion Detection
- local
- distributed
### Network-Based Intrusion Detection
- inline
- passive
- wireless
### Distributed or Hybrid
## Honeypots
- low interaction
- high interaction

# Firewall
## Types
### Packet Filtering Firewall
### Stateful Inspection Firewall
### Application Proxy Firewall
### Circuit-Level Proxy Firewall
## Basing
### Bastion Host
### Host-Based Firewall
### Network Device Firewall
### Virtual Firewall
### Personal Firewall
## VPN
## Topology
### Single Bastion Inline
### Double Bastion Inline
### Double Bastion T
### Distributed Firewall
## Intrusion Prevention System 
- Firewall+IDS

# Buffer Overflow
## Stack Buffer Overflow
## Shellcode
## Defending
### Compile-Time
#### High-Level Language
#### Safe Coding Techniques
- like certC
#### Language Extentions/Safe Libraries
#### Stack Protection
- canary
### Run-Time
#### Executable Address Space Protection
#### Address Space Layout Randomization
#### Guard Pages

# Software Security
## Handling Input
### Input Size
### Interpretation of Input
- validating input syntax
- alternate encoding
- validating numeric input
## Writing Safe Program Code
- problem variants
- additional debug code
- compilation/interpretation vulnerability
### Machine language code correspond to algorithm
### Manipulation of values
### Correct Data Interpretation
### Correct Use of Dynamic Memory
### Correct Use of Shared Memory
## OS and Other Programs Interactions
### Enviroment Variables
### Appropriate Least Privileges
### System Calls and Standard Library Functions
### Prevention of Race Conditions
### Safe Use of Temporary Files
### Interaction with other Programs
## Handling Output

# Operating System Security
## Hardening
### Initial Setup and Patching
- linux: package manager
- windows: windows update service
### Configure
#### Configure (or Remove) Services, Applications and Protocols
- linux: \etc, dotfiles
- windows: registry
#### Configure Users, Groups, Authentication and Resources
- all: DAC
- linux: /etc/passwd, /etc/group simpler
- windows: more complex
#### Additional Security
##### Host-Based firewall 
- linux: iptables for netfilter, IDS (better anomaly for syscalls)
- windows: antivirus (cause of DLL) and firewall (now included by default)
##### Logging
- linux: /dev/log, logrotate 
### Application Security
- linux: chroot jail
#### Test System Security
- all: checklist + external tools
## Security Maintenance
- Monitoring and analyzing logging information
- Performing regular backups
- Recovering from security compromises
- Regularly testing system security
- Patch and update all critical software
- Monitor and revise configuration as needed
## Virtualization
### Basics
### Hypervisors
#### Classification
#### HW Resources
#### Network Access 
### Containers
### Securing Virtualization 
- Ensure that the virtualization SW is properly secured
- Secure all elements of a full virtualization solution and maintain their security
- Restrict and protect administrator access to the virtualization solution
#### Hypervisor Security
- Guest OS isolation
- Guest OS monitoring
- Virtualized environment security: (aka snapshot and image management)
- hypervisor should be secured using a process similar to securing an OS
### Virtual Firewall
- VM Bastion Host
- VM Host-Based Firewall
- Hypervisor Firewall