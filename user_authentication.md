# User Authentication

## Digital User Authentication
- Identification: means used by an user to identify itself by presenting a credential (user ID)
- Authentication (verification): means used to establish the validity of the user's claimed identity

![Digital Identity Model](pictures/dig_id.png)

Every properly implemented method can provide secure user authentication, but every method has problems. So the strenght of an authentication system is determined by the number of factors.

### Risk Assestment
- **Assurance Level**: degree of certainty that an user has presented a credential that refers to his or her identity (from level 1, Low, to level 4, Very High)
- **Potential Impact**: degree of degradation in mission capability due to a breach of security (from Low to High)
- **Area of Risk**: for a given asset, must be determined by:
  - risk areas, or categories of impact
  - a mapping of the level of potential impact and the level of assurance, for each risk area

## Password Authentication (something known)
User provide:
- ID: used to determine 
  - authorization to access the system
  - authorization to access files (DAC)
  - privileges
- Password: used to authenticate the submitted ID

This authentication method uses a *Password File* indexed by user IDs (don't contain the passwords but their hashes). This is used by some of the attacks to this authentication method.

### Hashed Passwords: UNIX style
Widely used password security technique that uses *hashed passwords* and a *salt value*. 

![Salted Hashed Password](pictures/salt.png)

Salt values are useful against:
- multi-account attack: equals hashes are different passwords, so computation can be reused
- Dictionary attack: can't use precomputed hashes
- Rainbow table attack:  can't ure precomputed hashes

[comment]: <to review dict and rain> 
#### Dictionary Attack
Compute a large Dictionary of candidate passwords and relative hashes, than each eantry is tried against the password file.
The hash algorithm and the passwords'hashes must be known.

```
preComputation():
    for(each Word in Dictionary):
        store(DB,Word,hash(Word))


attack(password_hash H):
    w = lookup(DB, H)
    if (lookup succeeded):
        write ("pwd = ", w)
    else:
        write ("pwd not in Dictionary")
```

#### Rainbow Table Attack
Trade-off of space and time: 
- uses less computation that brute force, but more storage
- uses less storage that dictionary attack, but more computation

We need a function $`R:H \to P`$ that map back hashes to passwords. $`R`$ $`!=`$ $`H^{-1}`$

```
pre-computation():
    // choose a random set of initial passwords
    for ( 10^9 distinct P ): 
        for (p=P, n=0; n<1000; n++):
            //compute chains of length 1000
            //saving only head and tail
            k=h(p) 
            p=r(k) 
        // store chain start and end
        store(DB,P,p) 

attack(password_hash H):
    for (k=H; n=0; n<1000; n++)
        // starting from the hash in the password file
        p=r(k)
        // the objective is to check the hash is in one 
        // of the precomputed chain
        // comparing only with the head and the tail
        if lookup( DB, x, p ) then exit ("chain found!")
        k=h(p)
    exit ( "H is not in any chain of DB" )


```

![Rainbow Table](pictures/rainbow.png)

#### Password File Access Control
A way to prevent offline guessing attacks to passwords is to deny the access to the password file to an opponent keeping hashed password in a different file from the users IDs (the  shadow file). Operating system should protect the shadow file.

### Password Selection
A password protection system must complement access control measures wit password selection strategies to eliminate guessable passwords while allowing users to use memorable ones.
This can be done by: 
- User education
- Computer generated password
- Reactive password checking: system periodically tries to crack its own passwords
- Proactive password checking (or Complex password policy): system check if an user's selected password is strong enought

#### Proactive Password Checking
This can be done by:
- Rule enforcement: only passwords following the guidelines are accepted. It can allert crackers on which passwords not to try.
- Password Checker: Compile a dictionary of "bad" (blacklist) passwords that can't be selected by an user. It require lot of storage space to save the passwords and lot of time to search if a candidate password is in the blacklist. The dictionary must include also likely permutations to be effective, so it must be very large (tens of MB).

##### Bloom Filter
Given k independents hash functions mapping the inputs to integers between 0 and N-1:
- Preparation:
  - a *bit array* of N bits is initially set with all bits to 0
  - for each word in the dictionary of the forbidden words, the relative k hash values are calculated, the bits of the bit array corresponding to the outputs are set to 1
- Application:
  - when a new password is presented to the checker its k hashes are calculated
  - if all the corresponding bits of the array are equal to 1, the password is rejected
  - there will be false positives, but all words in the dictionary will be rejected

The probability of false positive is $`P \approx (1-e^{-kD/N})^k`$, with k hash algorithms, N bits of the array and D inserted words.
So P decreases as R=N/D increases.

Advantages:
- compression of dictionary
- fast check of passwords using only hash functions

## Token Authentication (something possessed)

### Memory Cards
- Can store but can't process data
- used alone provide **phisical access**
- used with a pin or a password provide **authentication**

### Smart Tokens
Classified by 4 non mutually exclusive dimensions:
- Physical characteristics: include a microprocessor.
  - can be a card (smart card)
  - can be a small calculator
- User interface: include a keypad and/or a display.
- Electronic interface: used to communicate with reader/writer
  - Contact
  - Contactless
- Authentication Protocol: 
  - Static: user authenticate to the token, the token authenticate the user to the system
  - Dynamic: token generates an one-time password that can be used for authentication
  - Challenge-response: the system generates a challenge, such as a random string of numbers, the smart token generates a response based on the challenge

#### Challenge-Response 
![Challenge-Response](pictures/challenge_response.png)

#### Smart-Cards
Microprocessor + Cyptography co-processor + Memory (RAM,ROM,ElectricallyEreasableROM).

![Smart Card](pictures/smart_card.png)
ATR=Answer To Reset, PTS=Protol Type Selection, APDU=Application Protocol Data Unit 

Electronic Identity Cards, a subset of the smart cards are now used as identity documents for citizens.
It has: 
- Personal Data
- Document Number
- Card Access Number (CAN): six-digit deciman printed on the card, can be used as a password
- Machine Readable Zone (MRZ): three lines of human- and machine-readable text on the back of the card, which may also be used as a password

##### Password Authenticated Connection Establishment (PACE)
Ensures that eID caard can't be read without explicit access control.
- For online applications: access performed using the 6-digit PIN (not the number printed on the card)
- For offline applications: either the MRZ or the CAN can be used 

![eID Authentication](pictures/eID_auth.png)

## Biometric Authentication (something that is or is done)
Based on unique physical characteristics used by pattern regcognition algorithms.
It is more complex and expensive that passwords and tokens.
Can be:
- Static: something the user is
  - fingerprints
  - hand geometry
  - face characteristics
  - retina and iris pattern
- Dynamic: something the user does
  - signature
  - voiceprint

### Enrollment
![Biometric Enrollment](pictures/biometric_enrollment.png)

### Verification/Identification
![Biometric Usage](pictures/biometric_usage.png)

### Testing
The complexity of physical characteristics led to the improbability of exact match.
To determine the closeliness of two template, a score (a single number) is generated to quantify the similarity between the saved template and the submitted one.
Testing a single user multiple times led to a probality density dunction (bell-shaped) and to an uncertainty interval.
The choice of the threshold become fundamental, but is dependent to the system requirements.

![Biometric Density](pictures/biometric_bell.png)

## Remote Authentication

### Challenge-Response Password Protocol 
![Challenge-Response Password](pictures/challenge_password.png)

### Challenge-Response Token Protocol
![Challenge-Response Token](pictures/challenge_token.png)

### Challenge-Response Static Biometric Protocol
![Challenge-Response Static Biometric](pictures/challenge_s_biometric.png)

### Challenge-Response Dynamic Biometric Protocol
![Challenge-Response Dynamic Biometric](pictures/challenge_d_biometric.png)

## Convenience 
Authenticator must be convenient to be effective, if it isn't it will not be used properly
- frequent password changes led the user to writhe down the password
- token remove the password problem, but can be forgotten
- biometric remove the token's problem, but has false positives

## Cost
The cost of an authentication system is quantified estimating the cost of the minimum-security implementation that makes the cost of the attack higher that the maximum potential gain.

The costs are: per-user(tokens), infrastructure (of the authentication system) and administrative (reset of a password).
An efficient authenticator reduces administrative costs.