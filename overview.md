# Overview

## Key Concepts
- **Confidentiality**: information is not disclosed to unauthorized individuals.
- **Integrity**: informations, programs and systems are changed or used only in a specified and authorized manner. 
- **Availability**: system works promptly and service is not denied to authorized users.
- **Authenticity**: assures that an entity or an object is genuine and able to be verified and trusted.
- **Accountability**: actions of an entity can be traced uniquely to that entity.

## Security Design Principles
- **Economy of mechanism**: the design should be as simple as possible.
- **Fail safe default**: a safe situation must be always detectable.
- **Complete mediation**: every access should be checked by the access control mechanism.
- **Open design**: the design should be open.
- **Separation of privileges**: multiple privileges should be needed to comlete a task.
- **Least privilege**: users and processes should have the least privilege needed to perform a task.
- **Least common mechanism**: design should minimize the functions shared by different users.
- **Psychological acceptability**: security procedures must reflect the user's mental model to not interfere with their work.
- **Isolation**: sesnsitive assets (access system, users data and security mechanisms) should be kept isolated.
- **Encapsulation**: procedures and data objects are encapsulated in the domain (like in Object-Oriented).
- **Modularity**: security functions are developed as separate and protected modules
- **Layering**: use of multiple and overlapping approaches.