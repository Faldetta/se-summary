# Intrusion Detection

## Intruders
> **Security Intrusion**
> Unauthorized act of bypassing the security mechanisms of a system

The use of some form of hacking/cracking by an intruder is a threath to the security.

### Motivations
- Cyber criminals (financial reward)
- Activists (hacktivist)
- State-sponsored organizations (spionage or sabotage)
- Others (challenge and/or curiosity)

### Skill Levels
- Apprentice: uses existing attack toolkits (script kiddies)
- Journeyman: extends attack toolkitsto exploit new vulnerabilities
- Master: write new powerful attack toolkits exploiting zero-day vulnerability

### Behaviour
1. Target acquisition and information gathering
2. Initial access
3. Privilege escalation
4. Information gathering or system exploit
5. Maintaining Access
6. Covering tracks 

## Detection
> **Intrusion Detection**
> HW or SW function that gathers and analyzes information from various areas within a computer or a network to identify possible security intrusions

Logical classes of components:
- Sensors: collect data
- Analyzers: determine if intrusion has occurred, can provide guidance about reactions
- User interface: enables user to view system's output

Classification:
- Host-based IDS: monitors a single host
- Network-based IDS: monitors network traffic
- Distributed or Hybrid IDS: combines info from HIDS and NIDS in a central analizer

Assumption:
behaviour of intruder differ from that  of a legitimate user in ways that can be quantified.

![Intrusion Behaviour](pictures/intrusion_bells.png)

## Analyst Approaches

### Anomaly Detection
Aims to define normal behaviour to determine differences from it, this make possible to detect zero-day attacks.
It suffers the difficulty in collecting and analyzing data and the high level of false allarm.

It consists of two phases:
- Training phase: develop a modell of legitimate user behaviour by collecting and processing sensors data from the normal operations.
- Detection phase: compare current observed behaviour with the model in order to classify it.

Different approaches can be used to define the model:
- Statistical: generate statistical profile
- Knowledge-based: classify the data with a set of rules
- Machine-learning: automatically generate a model by the use of data mining

### Signature or Heuristic Detection
**Signature**:
- match a collection of patterns of malicious data against data stored in a system or in transit over a network
- signature must be large enough to minimize false alarm rate 
- low cost in both time and resources, but an effort should be made to identify and review new malware (so it is unable to identify zero-days)

**Rule-based heuristic identification**:
- uses rules to identify intrusions
- rules are specific to machine/OS
- rules are developed by: 
  - analyzing attack tools and scripts 
  - using known intrusion scenarios and key events

## Host-Based Intrusion Detection (HIDS)
Monitors activity on the host to detect suspicious behaviour.

Sensors gather, filter and forward data to IDS analyser.

Data sources can be differents: 
- system call traces
- (audit) log files
- file integrity checksums
- registry access

Anomaly-based is used on linux due to the wide use of system calls, on windows is not that good due to the use of DLL.

Signature-based is commonly used in windows, mail and web application proxies.

Distributed HIDS results from the coordination and cooperation among IDSs permits to achieve a more effective defense than by using stand-alone IDSs on each host.
The issues in the design of a distributed HIDS deals with: 
- data formats
- integrity and confidentiality of sensors data
- use either a centralized or decentralized architecture

Components:
- Host agent module: background process collecting data on the host and sending these to the central manager
- LAN monitor agent module: like the host agent, exept that it analyzes LAN traffic
- Central management module: receives reports from host and LAN agents and correlate these reports to detect intrusions

![Distributed HIDS Architecture](pictures/distr_hids.png)

![HIDS Scheme](pictures/hids_scheme.png)

## Network-Based Intrusion Detection (NIDS)
Monitors traffic at selected points on a network examining traffic in real time (or close to it).
Analysis of traffic patterns may be done at the sensors, the management servers or a combination of the two

Differently from a HIDS which examines user and SW activity, NIDS examines packet traffic toward potentially vulnerable computer systems on a network (external intrusions).

A network sensor can be: 
- inline: inserted in a network segment so that the traffic must pass through the sensor; thi can block an attack when it is detected performing both detection and prevention
- passive: monitors a copy of the network traffic because the actual traffic don't pass through the device; thiv sensor is more efficient because don't add overhead and delay to transmissions but require a separate HW
- wireless: eg. incorporated into an access point

Passive Sensor:

![NIDS Passive Sensor](pictures/nids.png)

![NIDS Deployment](pictures/nids_deployment.png)

Signature detection is used for application, transport and network layer attacks, for policy violation and unauthorized access.

Anomaly detection is used for Dos, scanning attacks and worms.

Statefull Protocol Analysis (SPA) (anomaly detection) compare observed traffic with predetermined profiles; it requires high resource usage.

Logging is useful to refine intrusion detection parameters and algorithms, and to help security administrator to design prevention techniques.
 
## Distributed or Hybrid Intrusion Detection
Conventional IDS proplems:
- may not recognize new threath
- may not react rapidly enough
- may no detect slowly spreading attacks

So architecture has evoloved to involve distributed systems that cooperate to manage and coordinate intrusion detection.

Distributed or Hybrid IDSs are cooperating systems that can recognize attacks based on more subtle signs than conventional IDSs and then adapt quickly.

In a Distributed/Hybrid IDS an anomaly detector at a machine:
- looks for  evidence of unusual activity
- uses a peer-to-peer gossip protocol to inform other machines about its suspicion (probability of being under attack)
- if a machine receives enough of theese messages so that a threshold is exceeded, it assumes an attack is under way and responds

![Distributed IDS](pictures/distributed_ids.png)

- each host and network device is considered a sensor
- sensors exchange gossip informations
- three inputs guide the central system:
  - summary events: events from various sources collected and summarized by intermediate collection points
  - Distributed Detection and Inference events: alerts generated when the gossip traffic enables a platform to conclude that an attack is under way
  - Policy Enforcement Points events: PEP correlate distributed informations, local decisions and individual device actions to detect intrusions that may not be evident at the host level
- a central system is configured with a set of policies
- policies may be adapted basing on input from sensors
- central system communicates also collaborative policies to all platformsto adjust timing and content of collaborative gossip messages

## Intrusion Detection Exchange Format
To facilitate the development of distributed IDSs, standard message formats and exchange protocols are needed for sharing information of interest

Key elements: 

![Intrusion Detection Exchange Format](pictures/IDS_exchange.png)

## Honeypots
Decoy system designed to: 
- divert an attacker away from a critical system
- collect informations about attacker activity
- encourage the attacker to stay on the system long enough for administrator to respond

Hponeypots are resources fillled with fabbricated informations, so there isn't a legitimate reason to interact with it, except for an attacker.

Classification:
- Low interaction: SW package emulating a specific service or system, without executing a full version of those services or systems. It is a less realistic target, but often realistic enough to be efficient 
- High interaction: real system with a full OS, services and  applications. It is a more realistic target that may occupy an attacker for an extended period, but require lot of resources. If compromised could be used to initiate attacks.

![Honeypot](pictures/honeypot.png)

### HoneyFiles
Emulate legitimate documents with realistic, enticing names and, possibly, content.
Should not be accessed by legitimate users of a system, but rather act as a bait for intruders exploring a system.
