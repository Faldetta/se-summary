# Cryptographic Tools

## Symmetric Encryption

### Ingredients
- Plaintext: original message.
- Encryption algorithm: performs substitution and transformations on the plaintext.
- Secret key: other input of the encryption algorithm that determine the exact substitutions and transformations made by the algorithm.
- Ciphertext: scrambled message produced as output.
- Decryption algorithm: the encryption algorithm runned in reverse.

### Model
![symmetric encryption model](/pictures/sym_enc.png)

### Requirements
- Need of a strong encryption algorithm.
- Sender and receiver must obtain copies of the secret key in a secure way and the key must be kept secure.

### Approaches to Attack
- **Cryptanalityc**: rely on the characteristics of the algorithm, of a plaintext and of plaintext-ciphertext pairs to deduce a specific plaintext or the key being used.
- **Brute-Force**: Try all the possible keys on some ciphertext until a reasonable translation is obtained (in average half of the possible keys must be tried). Soe knowledge on the expected plaintext is needed to distinguish plaintet from garbled text.

### Block Cipher

#### Algorithms
- Data Encryption Standard (DES): no fatal weakness has been reported, however the key length (56 bit) is now inadequate.
- Triple DES (3DES): repeat DES three times using one (56 bit), two (112 bit) or three keys (168 bit).
- Advanced Encryption Standard (AES): stronger than 3DES in security and efficiency. 
Can be used with 128, 192 and 256 bit keys.

Tipically symmetric encryption is applied to data blocks of 64 or 128 bit.

#### Modes of Operations
Electronic Code Book (ECB): it is the simplest approach to multi-block encryption. Each block is encrypted using the same algorithm and the same key. For long messages may not be secure cause cryptanalisis could exploit block repetitions.

Cipher Block Chaining (CBC): to prevent the repetitions of blocks with the same ciphertext, it uses, as input for the algorithm, the XOR of the current plaintext and the preceding ciphertext block. The MAC, to be seen, is derived from the final block encryption.

### Stream Cipher

The most popular algorithm is RC4, for a good security 128 bit keys are used. It is used in SSL/TLS, WEP and WPA.

Unlike block cipher, that compute a block at the time, stream cipher processes the output countinously producing output one element (usually 1 byte) at the time.

![symmetric encryption model](/pictures/stream_cipher.png)

The key is input to a pseudorandom bit generator that produces a stream, called a keystream, of 8 bit numbers that are apparently random. The keystream is combined one byte at a time with the plaintext stream using the bitwise XOR operation.

### Block & Stream Cipher
| Block                                           | Stream                                                                          |
| ----------------------------------------------- | ------------------------------------------------------------------------------- |
| keys can be reused                              | faster and with less code                                                       |
| better when working with blocks of data (files) | better while encrypting stream of data (channel)                                |
|                                                 | XOR of cyphertext encrypted with the same key produce the XOR of the plaintexts |
### Key Distribution
Symmetric encryption require:
- the two parties must share the same key.
- the key must be protected.
- frequent key changes limit the amount of compromised data in case the attacker learns the key.

So the strenght of a cryptographic system rests with the ***key distribution***.

## Message Authentication 
While encryption protect against passive attack (eavesdropping), message authentication protects against active attacks (falsification) verifing its authenticity (of content and source).
Symmetric encryption is not suitable for data authentication cause blocks can be reordered without compromise the decryption pocedure.

### Message Authentication Code (MAC)
A small block of data, generated with a secret key, is appended to the message. The receiver must perform the same operation of the sender and check if the results match.

![MAC](/pictures/MAC.png)

MAC can be generated with the final block of DES or AES encryption.

### Message Digest
This technique relies on cryptographic hash functions, that accepts a variable lenght message as input and produce a fixed lenght message digest as output.
Unlike MAC, no encryption is used.

![Hash Function](/pictures/hash.png)

How it works:
- sender transmits the message with the relative digest.
- the receiver use the message to recalculate the digest and verify the aunthenticity comparing the digests.

**Avalanche effect**: small changes in the input drastically change the output.

Problems:
- collisions: different messages with the same digest (caused by the loss of info caused by the function).
- unprotected digest: the attacker can change the message and its digest $`\implies`$ the digest must be protected

#### Message Authentication using a Message Digest and Symmetric Encryption
![Digest with Symmetric](/pictures/digest_sym.png)

#### Message Authentication using a Message Digest and Public-key Encryption
![Digest with Public](/pictures/digest_pub.png)

#### Message Authentication a using a Message a Compare Digest and a Secret Key (no Encryption)
![Digest with Secret](/pictures/digest_sec.png)

#### Cryptographic Hash Function Requirements
- can be applied to a block of data of any size.
- produce a fixed length output.
- the hash is easy to compute given the input.
- **one-way**: given the digest h, finding an x such that $`H(x)=h`$ should be computationally infeasible.
- **weak collision resistant**: given a message x, finding an $`y`$ $`!=`$ $`x`$ such that $`H(y) = H(x)`$ should be computationally infeasible.
- **strong collision resistant**: it should be computationally infeasible to find a pair $`(x,y)`$ such that $`H(x)=H(y)`$.

NB: the strength of an hash function is proportional to the length of the hash code.

## Public-Key Encryption
- not more secure that symmetric encryption, is simply different.
- will not make symmetric encryption obsolete, due to the computational overhead of asymmetric one.

### Ingredients
- Plaintext: readable message.
- Encryption algorithm: performs transformations on the plaintext.
- Public and private key: pair ok keys that have been selecten so that if one is used for encryption, the other is used for decryption.
- Ciphertext: message produced as output; two different keys will produce two different ciphertexts
- Decryption algorithm: rebuild the original plaintext

Each user generate a pair of keys:
- public: placed in a public register, accessible to others.
- private: kept private

#### Encryption with public key
![Encryption with public key](/pictures/enc_pub.png)

#### Encryption with private key
![Encryption with private key](/pictures/enc_priv.png)

### Considerations
- Public key: provide confidentiality
    - only the intended receiver is able do decrypt because is the only one in possession of the required pivate key.
- Private key: provide origin authentication and data integrity
    - the receiver(s) is (are) able to decrypt the message with the sender public key.
    - only the sender private key can be used to modify the message.
    - origin authentication and data integrity depends on a variety of factors.

### Requirements
- **one-way**
    - $`Y=f(X)`$ easy
    - $`X=f^{-1}(Y)`$ infeasible
- **trap-door one-way**
    - $`Y=f_k(X)`$ easy with k and X known
    - $`X=f_k^{-1}(Y)`$ easy with k and Y known
    - $`X=f_k^{-1}(Y)`$ infeasible with Y known and k unknown 
In the case above, k is the trapdoor 

## Digital Signature and Key Management

### Digital Signature
Result of a cryptographic transformation of data that, when properly implemented, provides a mechanism (a data-dependent bit pattern created by the encryption of a hash code with a private key) to verify origin authentication, data integrity and signatory non-repudiation.

![Digital Signature](/pictures/dig_sign.png)

### Key Management

#### Public-Key Certificates (PKC)
Made of a public key plus an user ID of the key owner, with the whole block signed by a trusted third party (Certificate Authority (CA)).

![Public-Key Certificates](/pictures/cert.png)

The management of public-key certificates is responsability of a **Public-Key Infrastructure (PKI)**. A PKI is a technical and administrative infrastructure in place for creation, distribution and revocation of public-key certificates.

The PKC suffer the **trust problem** where the verification of the a chain of certificates (A is certified by B, B is certified by C, called **trust chain**) reach a self-signed certificate (owned by a Top-Level or Root CA). This is dangerous because it is a point of attack. 

When verifying a certificate, one must check also the trust chain and that the certificate has not been revoked.

A certificate can be revoked by:
- Certificate Revocation List (CRL): list of revoked certificates by a given CA, may be not up-to-date.
- On-line Certificate Status Protocol (OCSP): verification mediated by an OCSP server.

#### Symmetric Key Distribution
The public-key cryptography can be used to share a symmetric key securely.

#### Digital Envelopes
![Digital Envelope Creation](pictures/dig_env_creat.png)

![Digital Envelope Opening](pictures/dig_env_open.png)
